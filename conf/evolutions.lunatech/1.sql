# Lunatech schema creation

# --- !Ups
CREATE TABLE country
(
  id int,
  code character varying(4),
  name character varying(300),
  continent character varying(3),
  wikipedia_link character varying(300),
  keywords character varying(300),
  CONSTRAINT country_pkey PRIMARY KEY (id),
  CONSTRAINT uidx_country_code UNIQUE (code)
);
CREATE TABLE airport
(
  id integer,
  ident character varying(10),
  type character varying(20),
  name character varying(300),
  latitude_deg character varying(20),
  longitude_deg character varying(20),
  elevation_ft character varying(10),
  continent character varying(4),
  iso_country character varying(4) references country(code),
  iso_region  character varying(10),
  municipality character varying(300),
  scheduled_service character varying(4),
  gps_code  character varying(10),
  iata_code character varying(10),
  local_code  character varying(10),
  home_link character varying(300),
  wikipedia_link  character varying(300),
  keywords character varying(300),
  CONSTRAINT airport_pkey PRIMARY KEY (id)
);


ALTER TABLE airport ADD CONSTRAINT unique_ident UNIQUE (ident);
CREATE TABLE runway
(
  id  integer,
  airport_ref integer references airport(id),
  airport_ident character varying(10) references airport(ident),
  length_ft integer,
  width_ft  integer,
  surface character varying(300),
  lighted character varying(1),
  closed  character varying(1),
  le_ident  character varying(20),
  le_latitude_deg character varying(20),
  le_longitude_deg  character varying(20),
  le_elevation_ft character varying(10),
  le_heading_degT character varying(20),
  le_displaced_threshold_ft character varying(20),
  he_ident  character varying(20),
  he_latitude_deg character varying(20),
  he_longitude_deg  character varying(20),
  he_elevation_ft character varying(20),
  he_heading_degT character varying(20),
  he_displaced_threshold_ft character varying(20),
  CONSTRAINT runways_pkey PRIMARY KEY (id)


);


# --- !Downs

DROP TABLE runway;
DROP TABLE airport;
DROP TABLE country;