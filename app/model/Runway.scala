package model

case class Runway(
  id: Int,
  airportRef: Int,
  airportIdent: String,
  length: Int,
  width: Int,
  surface: String)

