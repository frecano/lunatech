package model

case class Airport(
  id: String,
  ident: String,
  typeValue: String,
  name: String,
  isoCountry: String)
