package model

case class Country(
  id: Int,
  code: String,
  name: String,
  continent: String,
  wikipedia_link: String,
  keywords: String
)

case class FullCountryResult(country: Country, airport: Airport, runway: Runway)

case class CountryWithAirportsCount(countryName: String, countryCode: String, numberOfAirports: Int)
case class RunwayTypesPerCountry(country: Country, runways: Set[String])
