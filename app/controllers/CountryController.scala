package controllers

import javax.inject.Inject
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.Forms.single
import play.api.mvc._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import services.CountryService

class CountryController @Inject()(cc: ControllerComponents, countryService: CountryService) extends AbstractController(cc) {

  val countrySearchForm = Form(single("identifier" -> text))

  def search() = Action.async { implicit request =>
    countrySearchForm.bindFromRequest().fold(
      formWithErrors => Future.successful(Results.Redirect(routes.HomeController.index())),
      identifier => {
        countryService.searchCountry(identifier).map { result =>
          Ok(views.html.country.countrySearch(result))
        }
      }
    )
  }


  def reports() = Action.async { implicit request =>
    for {
      (highest10, lowest10) <- countryService.topCountries
      runwayTypesPerCountry <- countryService.runwaysPerCountry
    } yield Ok(views.html.report.generalReport(highest10, lowest10, runwayTypesPerCountry))

  }
}
