package dao

import model.Airport
import play.api.db.slick.HasDatabaseConfigProvider
import slick.jdbc.JdbcProfile




private[dao] trait AirportTables extends HasDatabaseConfigProvider[JdbcProfile] {
  import driver.api._
  val airportsQeury = TableQuery[AirportTable]

  class AirportTable(tag: Tag) extends Table[Airport](tag, "airport") {
    def id = column[String]("id", O.PrimaryKey)
    def ident = column[String]("ident")
    def typeValue = column[String]("type")
    def name = column[String]("name")
    def latitudeDeg = column[String]("latitude_deg")
    def longitudeDeg = column[String]("longitude_deg")
    def elevationFt = column[String]("elevation_ft")
    def continent = column[String]("continent")
    def isoCountry = column[String]("iso_country")
    def isoRegion = column[String]("iso_region")
    def municipality = column[String]("municipality")
    def scheduled_service = column[String]("scheduled_service")
    def gpsCode = column[String]("gps_code")
    def iataCode = column[String]("iata_code")
    def localCode = column[String]("local_code")
    def homeLink = column[String]("home_link")
    def wikipediaLink = column[String]("wikipedia_link")
    def keywords = column[String]("keywords")

    def * = (id, ident, typeValue, name, isoCountry) <> (Airport.tupled, Airport.unapply)
  }

}