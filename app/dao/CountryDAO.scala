package dao

import com.google.inject.{ImplementedBy, Inject}
import model.{Country, CountryWithAirportsCount, FullCountryResult}
import play.api.db.slick.{DatabaseConfigProvider, HasDatabaseConfigProvider}
import play.api.mvc.{AbstractController, ControllerComponents}
import play.db.NamedDatabase
import scala.concurrent.{ExecutionContext, Future}
import slick.jdbc.JdbcProfile

@ImplementedBy(classOf[CountryDAOImpl])
trait CountryDAO {
  def searchFullCountry(criteria: String): Future[Seq[FullCountryResult]]
  def fetchTopCountriesWithMoreAirports: Future[Seq[CountryWithAirportsCount]]
  def fetchTopCountriesWithFewerAirports: Future[Seq[CountryWithAirportsCount]]
  def runwayTypesPerCountry: Future[Seq[(Country, String)]]

}

class CountryDAOImpl @Inject()(
  @NamedDatabase("lunatech") protected val dbConfigProvider: DatabaseConfigProvider,
  cc: ControllerComponents)(
  implicit ec: ExecutionContext)
  extends AbstractController(cc)
  with CountryTables
  with AirportTables
  with RunwayTables with CountryDAO{

  import driver.api._

  override def searchFullCountry(criteria: String) = {
    val countryQuery = countriesQuery.filter(row => (row.name like s"%$criteria%") ||
      (row.code === criteria))
    val join = (for {
      ((c, a), r) <- (countryQuery join airportsQeury on (_.code === _.isoCountry)
        join runwaysQuery on (_._2.ident === _.airportIdent))
    } yield (c, a, r))
    db.run(join.result).map(_.map(FullCountryResult.tupled))
  }

  override def fetchTopCountriesWithMoreAirports = {
    val highest10 = topCountriesCommon.sortBy(_._3.desc).take(10)
    db.run(highest10.result).map(_.map(CountryWithAirportsCount.tupled))
  }

  override def fetchTopCountriesWithFewerAirports = {
    val lowest10 = topCountriesCommon.sortBy(_._3.asc).take(10)
    db.run(lowest10.result).map(_.map(CountryWithAirportsCount.tupled))
  }

  override def runwayTypesPerCountry = {

    val join = (for {
      ((c, a), r) <- (countriesQuery.join(airportsQeury).on(_.code === _.isoCountry)
        join runwaysQuery on (_._2.ident === _.airportIdent))
    } yield (c, r))

    val query = join.map { case (country, runway) =>
      (country, runway.surface)
    }

    db.run(query.result)
  }

  private def topCountriesCommon = {
    val join = (for {
      (c, a) <- countriesQuery.join(airportsQeury).on(_.code === _.isoCountry)
    } yield (c, a)).groupBy(x=> (x._1.name, x._1.code))

    join.map { case (countryData, airports) =>
      (countryData._1, countryData._2, airports.length)
    }
  }
}


private[dao] trait CountryTables extends HasDatabaseConfigProvider[JdbcProfile]{
  import driver.api._
  val countriesQuery = TableQuery[CountryTable]

  class CountryTable(tag: Tag) extends Table[Country](tag, "country") {
    def id = column[Int]("id", O.PrimaryKey)
    def code = column[String]("code")
    def name = column[String]("name")
    def continent = column[String]("continent")
    def wikipediaLink = column[String]("wikipedia_link")
    def keywords = column[String]("keywords")

    def * = (id, code, name, continent, wikipediaLink, keywords) <> (Country.tupled, Country.unapply)
  }

}


