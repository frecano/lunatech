package dao

import com.google.inject.ImplementedBy
import model.Runway
import play.api.db.slick.HasDatabaseConfigProvider
import slick.driver.JdbcProfile


private[dao] trait RunwayTables extends HasDatabaseConfigProvider[JdbcProfile] {
  import driver.api._
  val runwaysQuery = TableQuery[RunwayTable]

  class RunwayTable(tag: Tag) extends Table[Runway](tag, "runway") {

    def id = column[Int]("id", O.PrimaryKey)
    def airportRef = column[Int]("airport_ref")
    def airportIdent = column[String]("airport_ident")
    def length = column[Int]("length_ft")
    def width = column[Int]("width_ft")
    def surface = column[String]("surface")
    def lighted = column[String]("lighted")
    def closed = column[String]("closed")
    def le_ident = column[String]("le_ident")
    def le_latitude_deg = column[String]("le_latitude_deg")
    def le_longitude_deg = column[String]("le_longitude_deg")
    def le_elevation_ft = column[String]("le_elevation_ft")
    def le_heading_degT = column[String]("le_heading_degT")
    def le_displaced_threshold_ft = column[String]("le_displaced_threshold_ft")
    def he_ident = column[String]("he_ident")
    def he_latitude_deg = column[String]("he_latitude_deg")
    def he_longitude_deg = column[String]("he_longitude_deg")
    def he_elevation_ft = column[String]("he_elevation_ft")
    def he_heading_degT = column[String]("he_heading_degT")
    def he_displaced_threshold_ft = column[String]("he_displaced_threshold_ft")


    def * = (id, airportRef, airportIdent, length, width, surface) <> (Runway.tupled, Runway.unapply)
  }

}


