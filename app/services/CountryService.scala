package services

import com.google.inject.{ImplementedBy, Inject}
import dao.CountryDAO
import model.{CountryWithAirportsCount, FullCountryResult, RunwayTypesPerCountry}
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

@ImplementedBy(classOf[CountryServiceImpl])
trait CountryService {
  def topCountries: Future[(Seq[CountryWithAirportsCount], Seq[CountryWithAirportsCount])]
  def runwaysPerCountry: Future[Seq[RunwayTypesPerCountry]]
  //def commonRunwayIdentifications: CommonRunways
  def searchCountry(criteria: String): Future[Seq[FullCountryResult]]
}


class CountryServiceImpl @Inject()(countryDAO: CountryDAO) extends CountryService {

  def searchCountry(criteria: String) = countryDAO.searchFullCountry(criteria)

  override def topCountries = {
    for {
      highest10 <- countryDAO.fetchTopCountriesWithMoreAirports
      lowest10 <- countryDAO.fetchTopCountriesWithFewerAirports
    } yield (highest10, lowest10)
  }

  override def runwaysPerCountry = {
    countryDAO.runwayTypesPerCountry.map{ a =>
      a.groupBy(_._1).mapValues{ seq =>(seq map { _._2 }).toSet }.map(RunwayTypesPerCountry.tupled).toSeq

    }

  }
}
